# -*- coding: utf-8 -*-
# Implementation of Wang et al 2017: Automatic Brain Tumor Segmentation using Cascaded Anisotropic Convolutional Neural Networks. https://arxiv.org/abs/1709.00382

# Author: Guotai Wang
# Copyright (c) 2017-2018 University College London, United Kingdom. All rights reserved.
# http://cmictig.cs.ucl.ac.uk
#
# Distributed under the BSD-3 licence. Please see the file licence.txt
# This software is not certified for clinical use.
#
from __future__ import absolute_import, print_function

import os
import random
import nibabel
import numpy as np
from scipy import ndimage
from util.data_process import *

# TODO: add rotation augmentation and return both orignal and rotated images when calling 'get_image_data_with_name'

class Inf_DataLoader():
    def __init__(self, config):
        """
        Initialize the calss instance
        inputs:
            config: a dictionary representing parameters
        """
        self.config    = config
        self.data_root = config['data_root'] if type(config['data_root']) is list else [config['data_root']]
        self.modality_postfix     = config.get('modality_postfix', ['flair','t1', 't1ce', 't2'])
        self.intensity_normalize  = config.get('intensity_normalize', [True, True, True, True])
        self.with_ground_truth    = config.get('with_ground_truth', False)
        self.label_convert_source = config.get('label_convert_source', None)
        self.label_convert_target = config.get('label_convert_target', None)
        self.label_postfix = config.get('label_postfix', 'seg')
        self.file_postfix  = config.get('file_postfix', 'nii.gz')
        self.data_names    = config.get('data_names', None)
        self.data_num      = config.get('data_num', None)
        self.data_resize   = config.get('data_resize', None)
        self.with_flip     = config.get('with_flip', False)

        # TODO: add rotation transformation class
        # TODO DONE: please check
        ord_dict = OrderedDict()
        ord_dict['rotation_x'] = 45
        ord_dict['rotation_y'] = 0
        ord_dict['rotation_z'] = 0
        target_dim = 240
        self.rotation_3d = Rotation3d(ord_dict, target_dim, pad_mode='random')

        if(self.label_convert_source and self.label_convert_target):
            assert(len(self.label_convert_source) == len(self.label_convert_target))
            
    def __get_patient_names(self):
        """
        get the list of patient names, if self.data_names id not None, then load patient 
        names from that file, otherwise search all the names automatically in data_root
        """
        # use pre-defined patient names
        if(self.data_names is not None):
            assert(os.path.isfile(self.data_names))
            with open(self.data_names) as f:
                content = f.readlines()
            patient_names = [x.strip() for x in content]
        # use all the patient names in data_root
        else:
            patient_names = os.listdir(self.data_root[0])
            patient_names = [name for name in patient_names if 'brats' in name.lower()]
        return patient_names

    def __load_one_volume(self, patient_name, mod):
        patient_dir = os.path.join(self.data_root[0], patient_name)
        # for bats17
        if('nii' in self.file_postfix):
            image_names = os.listdir(patient_dir)
            volume_name = None
            for image_name in image_names:
                if(mod + '.' in image_name):
                    volume_name = image_name
                    break
        # for brats15
        else:
            img_file_dirs = os.listdir(patient_dir)
            volume_name  = None
            for img_file_dir in img_file_dirs:
                if(mod+'.' in img_file_dir):
                    volume_name = img_file_dir + '/' + img_file_dir + '.' + self.file_postfix
                    break
        assert(volume_name is not None)
        volume_name = os.path.join(patient_dir, volume_name)
        volume = load_3d_volume_as_array(volume_name)

        # TODO: apply rotation transform
        # TODO DONE: please check
        volume_r = self.rotation_3d.map(volume)

        return (volume, volume_r), volume_name

    def load_data(self):
        """
        load all the training/testing data
        """
        self.patient_names = self.__get_patient_names()
        assert(len(self.patient_names)  > 0)
        ImageNames = []
        X = []
        W = []
        Y = []
        bbox  = []
        in_size = []
        # TODO DONE
        X_r = []
        W_r = []
        Y = []
        bbox_r = []
        in_size_r = []

        data_num = self.data_num if (self.data_num is not None) else len(self.patient_names)
        for i in range(data_num):
            volume_list = []
            volume_name_list = []
            # TODO DONE
            volume_r_list = []
            for mod_idx in range(len(self.modality_postfix)):
                (volume, volume_r), volume_name = self.__load_one_volume(self.patient_names[i], self.modality_postfix[mod_idx])
                if(mod_idx == 0):
                    margin = 5
                    bbmin, bbmax = get_ND_bounding_box(volume, margin)
                    volume_size  = volume.shape
                    # TODO DONE
                    bbmin_r, bbmax_r = get_ND_bounding_box(volume_r, margin)
                    volume_size_r = volume_r.shape
                volume = crop_ND_volume_with_bounding_box(volume, bbmin, bbmax)
                # TODO DONE
                volume_r = crop_ND_volume_with_bounding_box(volume_r, bbmin_r, bbmax_r)
                if(mod_idx ==0):
                    weight = np.asarray(volume > 0, np.float32)
                    # TODO DONE
                    weight_r = np.asarray(volume_r > 0, np.float32)
                volume_list.append(volume)
                volume_name_list.append(volume_name)
                # TODO DONE
                volume_r_list.append(volume_r)
            ImageNames.append(volume_name_list)
            X.append(volume_list)
            W.append(weight)
            bbox.append([bbmin, bbmax])
            in_size.append(volume_size)
            # TODO DONE
            X_r.append(volume_r_list)
            W_r.append(weight_r)
            bbox_r.append([bbmin_r, bbmax_r])
            in_size_r.append(volume_size_r)

            if(self.with_ground_truth):
                label, _ = self.__load_one_volume(self.patient_names[i], self.label_postfix)
                label = crop_ND_volume_with_bounding_box(label, bbmin, bbmax)
                if(self.data_resize):
                    label = resize_ND_volume_to_given_shape(label, self.data_resize, 0)
                Y.append(label)
            if((i+1)%50 == 0 or (i+1) == data_num):
                print('Data load, {0:}% finished'.format((i+1)*100.0/data_num))
        self.image_names = ImageNames
        self.data   = X
        self.weight = W
        self.label  = Y
        self.bbox   = bbox
        self.in_size= in_size
        # TODO DONE
        self.data_r = X_r
        self.weight_r = W_r
        self.bbox_r = bbox_r
        self.in_size_r = in_size_r

    
    def get_total_image_number(self):
        """
        get the toal number of images
        """
        return len(self.data)
    
    def get_image_data_with_name(self, i):
        """
        Used for testing, get one image data and patient name
        """
        return ([self.data[i], self.weight[i], self.patient_names[i], self.image_names[i], self.bbox[i], self.in_size[i]],
                [self.data_r[i], self.weight_r[i], self.patient_names[i], self.image_names[i], self.bbox_r[i], self.in_size_r[i]])

