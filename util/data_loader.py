# -*- coding: utf-8 -*-
# Implementation of Wang et al 2017: Automatic Brain Tumor Segmentation using Cascaded Anisotropic Convolutional Neural Networks. https://arxiv.org/abs/1709.00382

# Author: Guotai Wang
# Copyright (c) 2017-2018 University College London, United Kingdom. All rights reserved.
# http://cmictig.cs.ucl.ac.uk
#
# Distributed under the BSD-3 licence. Please see the file licence.txt
# This software is not certified for clinical use.
#
from __future__ import absolute_import, print_function

import os
import random
import nibabel
import numpy as np
from scipy import ndimage
from util.data_process import *
from collections import OrderedDict


class DataLoader():
    def __init__(self, config):
        """
        Initialize the calss instance
        inputs:
            config: a dictionary representing parameters
        """
        self.config = config
        self.data_root = config['data_root'] if type(config['data_root']) is list else [config['data_root']]
        self.modality_postfix = config.get('modality_postfix', ['flair', 't1', 't1ce', 't2'])
        self.intensity_normalize = config.get('intensity_normalize', [True, True, True, True])
        self.with_ground_truth = config.get('with_ground_truth', False)
        self.label_convert_source = config.get('label_convert_source', None)
        self.label_convert_target = config.get('label_convert_target', None)
        self.label_postfix = config.get('label_postfix', 'seg')
        self.file_postfix = config.get('file_postfix', 'nii.gz')
        self.data_names = config.get('data_names', None)
        self.data_num = config.get('data_num', None)
        self.data_resize = config.get('data_resize', None)
        self.with_flip = config.get('with_flip', False)
        self.train_with_roi_patch = self.config.get('train_with_roi_patch', False)
        self.load_data_shape = config['load_data_shape']
        self.load_label_shape = config['load_label_shape']
        self.data_shape = config['data_shape']
        self.label_shape = config['label_shape']
        self.roi_lean = config.get('roi_lean', False)
        if self.roi_lean:
            ord_dict = OrderedDict()
            ord_dict['rotation_x'] = config['roi_lean_degree'][0]
            ord_dict['rotation_y'] = config['roi_lean_degree'][1]
            ord_dict['rotation_z'] = config['roi_lean_degree'][2]
            target_dim = 240
            self.rotation_3d = Rotation3d(ord_dict, target_dim, pad_mode='constant')

        if (self.label_convert_source and self.label_convert_target):
            assert (len(self.label_convert_source) == len(self.label_convert_target))

    def __get_patient_names(self):
        """
        get the list of patient names, if self.data_names id not None, then load patient
        names from that file, otherwise search all the names automatically in data_root
        """
        # use pre-defined patient names
        if (self.data_names is not None):
            assert (os.path.isfile(self.data_names))
            with open(self.data_names) as f:
                content = f.readlines()
            patient_names = [x.strip() for x in content]
        # use all the patient names in data_root
        else:
            patient_names = os.listdir(self.data_root[0])
            patient_names = [name for name in patient_names if 'brats' in name.lower()]
        return patient_names

    def __load_one_volume(self, patient_name, mod):
        patient_dir = os.path.join(self.data_root[0], patient_name)
        # for bats17
        if ('nii' in self.file_postfix):
            image_names = os.listdir(patient_dir)
            volume_name = None
            for image_name in image_names:
                if (mod + '.' in image_name):
                    volume_name = image_name
                    break
        # for brats15
        else:
            img_file_dirs = os.listdir(patient_dir)
            volume_name = None
            for img_file_dir in img_file_dirs:
                if (mod + '.' in img_file_dir):
                    volume_name = img_file_dir + '/' + img_file_dir + '.' + self.file_postfix
                    break
        assert (volume_name is not None)
        volume_name = os.path.join(patient_dir, volume_name)
        volume = load_3d_volume_as_array(volume_name)
        # rotate
        if self.roi_lean_degree:
            volume = self.rotation_3d.map(volume)
        return volume, volume_name

    def load_data_train(self):
        """
        load all the training/testing data
        """
        self.patient_names = self.__get_patient_names()
        assert (len(self.patient_names) > 0)
        self.roi_lean_degree = self.config.get('roi_lean_degree')

        load_data_shape = self.load_data_shape
        load_label_shape = self.load_label_shape
        down_sample_rate = self.config.get('down_sample_rate', 1.0)
        data_slice_number = load_data_shape[0]
        label_slice_number = load_label_shape[0]
        batch_sample_model = self.config.get('batch_sample_model', ('valid', 'valid', 'valid'))
        keep_roi_outside = self.config.get('keep_roi_outside', False)
        batch_slice_direction = self.config.get('batch_slice_direction', 'axial')
        train_with_roi_patch = self.train_with_roi_patch

        if (train_with_roi_patch):
            label_roi_mask = self.config['label_roi_mask']
            roi_patch_margin = self.config['roi_patch_margin']

        if (self.with_flip):
            flip = random.random() > 0.5
        else:
            flip = False

        if (train_with_roi_patch):
            label_roi_mask = self.config['label_roi_mask']
            roi_patch_margin = self.config['roi_patch_margin']

        slice_direction = batch_slice_direction
        if (slice_direction == 'random'):
            directions = ['axial', 'sagittal', 'coronal']
            idx = random.randint(0, 2)
            slice_direction = directions[idx]

        ImageNames = []
        X = []
        W = []
        Y = []
        data_num = self.data_num if (self.data_num is not None) else len(self.patient_names)

        for i in range(data_num):
            volume_list = []
            volume_name_list = []
            for mod_idx in range(len(self.modality_postfix)):
                volume, volume_name = self.__load_one_volume(self.patient_names[i], self.modality_postfix[mod_idx])
                if (mod_idx == 0):
                    margin = 5
                    bbmin, bbmax = get_ND_bounding_box(volume, margin)
                    volume_size = volume.shape
                volume = crop_ND_volume_with_bounding_box(volume, bbmin, bbmax)
                if (self.data_resize):
                    volume = resize_ND_volume_to_given_shape(volume, self.data_resize, 1)
                if (mod_idx == 0):
                    weight = np.asarray(volume > 0, np.float32)
                if (self.intensity_normalize[mod_idx]):
                    volume = itensity_normalize_one_volume(volume)
                volume_list.append(volume)
                volume_name_list.append(volume_name)
            boundingbox = None

            label, _ = self.__load_one_volume(self.patient_names[i], self.label_postfix)
            label = crop_ND_volume_with_bounding_box(label, bbmin, bbmax)
            if (self.data_resize):
                label = resize_ND_volume_to_given_shape(label, self.data_resize, 0)

            if (train_with_roi_patch):
                mask_volume = np.zeros_like(label)
                for mask_label in label_roi_mask:
                    mask_volume = mask_volume + (label == mask_label)
                [d_idxes, h_idxes, w_idxes] = np.nonzero(mask_volume)
                [Dd, Dh, Dw] = label.shape
                mind = max(d_idxes.min() - roi_patch_margin, 0)
                maxd = min(d_idxes.max() + roi_patch_margin, Dd)
                minh = max(h_idxes.min() - roi_patch_margin, 0)
                maxh = min(h_idxes.max() + roi_patch_margin, Dh)
                minw = max(w_idxes.min() - roi_patch_margin, 0)
                maxw = min(w_idxes.max() + roi_patch_margin, Dw)
                if (keep_roi_outside):
                    boundingbox = [mind, maxd, minh, maxh, minw, maxw]
                else:
                    for idx in range(len(volume_list)):
                        volume_list[idx] = volume_list[idx][np.ix_(range(mind, maxd),
                                                                   range(minh, maxh),
                                                                   range(minw, maxw))]
                    weight = weight[np.ix_(range(mind, maxd),
                                            range(minh, maxh),
                                            range(minw, maxw))]
                    label = label[np.ix_(range(mind, maxd),
                                         range(minh, maxh),
                                         range(minw, maxw))]

            if (self.label_convert_source and self.label_convert_target):
                label = [convert_label(label, self.label_convert_source, self.label_convert_target)]

            transposed_volumes = transpose_volumes(volume_list, slice_direction)
            volume_shape = transposed_volumes[0].shape
            sub_data_shape = [data_slice_number, load_data_shape[1], load_data_shape[2]]
            sub_label_shape = [label_slice_number, load_label_shape[1], load_label_shape[2]]
            center_point = get_random_roi_sampling_center(volume_shape, sub_label_shape, batch_sample_model,
                                                          boundingbox)
            sub_data = []
            for moda in range(len(transposed_volumes)):
                sub_data_moda = extract_roi_from_volume(transposed_volumes[moda], center_point, sub_data_shape)
                if (flip):
                    sub_data_moda = np.flip(sub_data_moda, -1)
                if (down_sample_rate != 1.0):
                    sub_data_moda = ndimage.interpolation.zoom(sub_data_moda, 1.0 / down_sample_rate, order=1)
                sub_data.append(sub_data_moda)
            sub_data = np.asarray(sub_data)

            transposed_weight = np.asarray(transpose_volumes([weight], slice_direction)).squeeze()
            sub_weight = extract_roi_from_volume(transposed_weight,
                                                 center_point,
                                                 sub_label_shape,
                                                 fill='zero')
            if (flip):
                sub_weight = np.flip(sub_weight, -1)
            if (down_sample_rate != 1.0):
                sub_weight = ndimage.interpolation.zoom(sub_weight, 1.0 / down_sample_rate, order=1)

            tranposed_label = transpose_volumes(label, slice_direction)
            sub_label = extract_roi_from_volume(tranposed_label[0],
                                                center_point,
                                                sub_label_shape,
                                                fill='zero')
            if (flip):
                sub_label = np.flip(sub_label, -1)
            if (down_sample_rate != 1.0):
                sub_label = ndimage.interpolation.zoom(sub_label, 1.0 / down_sample_rate, order=0)

            ImageNames.append(volume_name_list)
            X.append(sub_data)
            W.append(sub_weight)
            Y.append(sub_label)

            if ((i + 1) % 50 == 0 or (i + 1) == data_num):
                print('Data load, {0:}% finished'.format((i + 1) * 100.0 / data_num))

        self.image_names = ImageNames
        self.data = X
        self.weight = W
        self.label = Y

    def get_subimage_train_batch(self):
        """
        sample a batch of image patches for segmentation. Only used for training
        """
        flag = False
        while (flag == False):
            batch = self.__get_one_train_batch()
            labels = batch['labels']
            if (labels.sum() > 0):
                flag = True
        return batch

    def __get_one_train_batch(self):
        """
        get a batch from training data
        """
        # return batch size: [batch_size, slice_num, slice_h, slice_w, moda_chnl]
        batch_size = self.config['batch_size']
        batch_sample_model = self.config.get('batch_sample_model', ('full', 'valid', 'valid'))
        keep_roi_outside = self.config.get('keep_roi_outside', False)
        train_with_roi_patch = self.train_with_roi_patch
        data_shape = self.data_shape
        label_shape = self.label_shape
        data_slice_number = data_shape[0]
        label_slice_number = label_shape[0]
        data_batch = []
        weight_batch = []
        label_batch = []

        if (train_with_roi_patch):
            label_roi_mask = self.config['label_roi_mask']
            roi_patch_margin = 0

        assert (len(self.patient_names) > 0)

        for i in range(batch_size):
            patient_id = random.randint(0, len(self.data) - 1)
            data = self.data[patient_id].tolist()
            weight = self.weight[patient_id]
            label = self.label[patient_id]
            boundingbox = None

            if (train_with_roi_patch):
                [d_idxes, h_idxes, w_idxes] = np.nonzero(label)
                [Dd, Dh, Dw] = label.shape
                if len(d_idxes):
                    mind = max(d_idxes.min() - roi_patch_margin, 0)
                    maxd = min(d_idxes.max() + roi_patch_margin, Dd)
                    minh = max(h_idxes.min() - roi_patch_margin, 0)
                    maxh = min(h_idxes.max() + roi_patch_margin, Dh)
                    minw = max(w_idxes.min() - roi_patch_margin, 0)
                    maxw = min(w_idxes.max() + roi_patch_margin, Dw)
                else:
                    mind = 0
                    maxd = Dd
                    minh = 0
                    maxh = Dh
                    minw = 0
                    maxw = Dw

                if (keep_roi_outside):
                    boundingbox = [mind, maxd, minh, maxh, minw, maxw]
                else:
                    for idx in range(len(data)):
                        data[idx] = np.asarray(data[idx])[np.ix_(range(mind, maxd),
                                                                 range(minh, maxh),
                                                                 range(minw, maxw))]
                    data = np.asarray(data)
                    weight = weight[np.ix_(range(mind, maxd),
                                           range(minh, maxh),
                                           range(minw, maxw))]
                    label = label[np.ix_(range(mind, maxd),
                                         range(minh, maxh),
                                         range(minw, maxw))]

            volume_shape = np.asarray(data[0]).shape
            sub_data_shape = [data_slice_number, data_shape[1], data_shape[2]]
            sub_label_shape = [label_slice_number, label_shape[1], label_shape[2]]
            center_point = get_random_roi_sampling_center(volume_shape, sub_label_shape, batch_sample_model,
                                                          boundingbox)

            sub_data = []
            for moda in range(len(data)):
                sub_data_moda = extract_roi_from_volume(np.asarray(data[moda]),
                                                        center_point,
                                                        sub_data_shape)
                sub_data.append(sub_data_moda)
            sub_data = np.asarray(sub_data)

            sub_weight = extract_roi_from_volume(weight,
                                                 center_point,
                                                 sub_label_shape,
                                                 fill='zero')

            sub_label = extract_roi_from_volume(label,
                                                center_point,
                                                sub_label_shape,
                                                fill='zero')
            data_batch.append(sub_data)
            weight_batch.append([sub_weight])
            label_batch.append([sub_label])

        data_batch = np.asarray(data_batch, np.float32)
        weight_batch = np.asarray(weight_batch, np.float32)
        label_batch = np.asarray(label_batch, np.int64)
        batch = {}
        batch['images'] = np.transpose(data_batch, [0, 2, 3, 4, 1])
        batch['weights'] = np.transpose(weight_batch, [0, 2, 3, 4, 1])
        batch['labels'] = np.transpose(label_batch, [0, 2, 3, 4, 1])

        return batch

    def get_total_image_number(self):
        """
        get the toal number of images
        """
        return len(self.data)

    def get_image_data_with_name(self, i):
        """
        Used for testing, get one image data and patient name
        """
        return [self.data[i], self.weight[i], self.patient_names[i], self.image_names[i], self.bbox[i], self.in_size[i]]
