[data]
data_root             = /home/shanlins/Projects/Data/brain_tumor/MICCAI_BraTS17_Data_Training
data_names            = config17/train_names_all.txt
modality_postfix      = [flair, t1, t1ce, t2]
label_postfix         = seg
file_postfix          = nii.gz
with_ground_truth     = True
batch_size            = 5
load_data_shape       = [51, 96, 96, 4]
load_label_shape      = [51, 96, 96, 1]
data_shape            = [19, 96, 96, 4]
label_shape           = [11, 96, 96, 1]
label_convert_source  = [0, 1, 2, 4]
label_convert_target  = [0, 1, 0, 1] 
batch_slice_direction = sagittal
train_with_roi_patch  = True
roi_lean              =
roi_lean_degree       = 
label_roi_mask        = [1,2,4]
roi_patch_margin      = 5

[network]
net_type            = MSNet
net_name            = MSNet_TC32sg
downsample_twice    = True
class_num           = 2

[training]
learning_rate      = 1e-3
decay              = 1e-7
maximal_iteration  = 10000
snapshot_iteration = 5000
start_iteration    = 0
test_iteration     = 100
test_step          = 10
model_pre_trained  = 
model_save_prefix  = model17/msnet_tc32sg
