# -*- coding: utf-8 -*-
# Implementation of Wang et al 2017: Automatic Brain Tumor Segmentation using Cascaded Anisotropic Convolutional Neural Networks. https://arxiv.org/abs/1709.00382

# Author: Guotai Wang
# Copyright (c) 2017-2018 University College London, United Kingdom. All rights reserved.
# http://cmictig.cs.ucl.ac.uk
#
# Distributed under the BSD-3 licence. Please see the file licence.txt
# This software is not certified for clinical use.
#

# TODO INSTRUCTION:
# 1. build the rotated models, and restore them
# 2. use the new inference data loader, called Inf_Dataloader
# 3. get the rotated predict results from 3 stages
# 4. fuse the rotated results and original results

from __future__ import absolute_import, print_function
import numpy as np
from scipy import ndimage
import time
import os
import sys
import tensorflow as tf
from util.inf_data_loader import *
from util.data_process import *
from util.train_test_func import *
from util.parse_config import parse_config
from train import NetFactory

def test(config_file):
    # 1, load configure file
    config = parse_config(config_file)
    config_data = config['data']
    config_net1 = config.get('network1', None)
    config_net2 = config.get('network2', None)
    config_net3 = config.get('network3', None)
    config_test = config['testing']  
    batch_size  = config_test.get('batch_size', 5)
    
    # 2.1, network for whole tumor
    config_net1ax = config['network1ax']
    config_net1sg = config['network1sg']
    config_net1cr = config['network1cr']
    # TODO: add three more configs -- done
    config_net1rax = config['network1rax']
    config_net1rsg = config['network1rsg']
    config_net1rcr = config['network1rcr']

    # construct graph for 1st network axial
    net_type1ax    = config_net1ax['net_type']
    net_name1ax    = config_net1ax['net_name']
    data_shape1ax  = config_net1ax['data_shape']
    label_shape1ax = config_net1ax['label_shape']
    class_num1ax   = config_net1ax['class_num']

    full_data_shape1ax = [batch_size] + data_shape1ax
    x1ax = tf.placeholder(tf.float32, shape = full_data_shape1ax)
    net_class1ax = NetFactory.create(net_type1ax)
    net1ax = net_class1ax(num_classes = class_num1ax,w_regularizer = None,
                b_regularizer = None, name = net_name1ax)
    net1ax.set_params(config_net1ax)
    predicty1ax = net1ax(x1ax, is_training = True)
    proby1ax = tf.nn.softmax(predicty1ax)

    # construct graph for 1st network sagittal
    net_type1sg    = config_net1sg['net_type']
    net_name1sg    = config_net1sg['net_name']
    data_shape1sg  = config_net1sg['data_shape']
    label_shape1sg = config_net1sg['label_shape']
    class_num1sg   = config_net1sg['class_num']

    full_data_shape1sg = [batch_size] + data_shape1sg
    x1sg = tf.placeholder(tf.float32, shape = full_data_shape1sg)
    net_class1sg = NetFactory.create(net_type1sg)
    net1sg = net_class1sg(num_classes = class_num1sg,w_regularizer = None,
                b_regularizer = None, name = net_name1sg)
    net1sg.set_params(config_net1sg)
    predicty1sg = net1sg(x1sg, is_training = True)
    proby1sg = tf.nn.softmax(predicty1sg)

    # construct graph for 1st network corogal
    net_type1cr    = config_net1cr['net_type']
    net_name1cr    = config_net1cr['net_name']
    data_shape1cr  = config_net1cr['data_shape']
    label_shape1cr = config_net1cr['label_shape']
    class_num1cr   = config_net1cr['class_num']

    full_data_shape1cr = [batch_size] + data_shape1cr
    x1cr = tf.placeholder(tf.float32, shape = full_data_shape1cr)
    net_class1cr = NetFactory.create(net_type1cr)
    net1cr = net_class1cr(num_classes = class_num1cr,w_regularizer = None,
                b_regularizer = None, name = net_name1cr)
    net1cr.set_params(config_net1cr)
    predicty1cr = net1cr(x1cr, is_training = True)
    proby1cr = tf.nn.softmax(predicty1cr)

    # TODO: construct graph for 1st network rotated axial -- done
    net_type1rax    = config_net1rax['net_type']
    net_name1rax    = config_net1rax['net_name']
    data_shape1rax  = config_net1rax['data_shape']
    label_shape1rax = config_net1rax['label_shape']
    class_num1rax   = config_net1rax['class_num']

    full_data_shape1rax = [batch_size] + data_shape1rax
    x1rax = tf.placeholder(tf.float32, shape = full_data_shape1rax)
    net_class1rax = NetFactory.create(net_type1rax)
    net1rax = net_class1rax(num_classes = class_num1rax,w_regularizer = None,
                b_regularizer = None, name = net_name1rax)
    net1rax.set_params(config_net1rax)
    predicty1rax = net1rax(x1rax, is_training = True)
    proby1rax = tf.nn.softmax(predicty1rax)

    # TODO: construct graph for 1st network rotated sagittal
    net_type1rsg    = config_net1rsg['net_type']
    net_name1rsg    = config_net1rsg['net_name']
    data_shape1rsg  = config_net1rsg['data_shape']
    label_shape1rsg = config_net1rsg['label_shape']
    class_num1rsg   = config_net1rsg['class_num']

    full_data_shape1rsg = [batch_size] + data_shape1rsg
    x1rsg = tf.placeholder(tf.float32, shape = full_data_shape1rsg)
    net_class1rsg = NetFactory.create(net_type1rsg)
    net1rsg = net_class1rsg(num_classes = class_num1rsg,w_regularizer = None,
                b_regularizer = None, name = net_name1rsg)
    net1rsg.set_params(config_net1rsg)
    predicty1rsg = net1rsg(x1rsg, is_training = True)
    proby1rsg = tf.nn.softmax(predicty1rsg)

    # TODO: construct graph for 1st network rotated corogal
    net_type1rcr    = config_net1rcr['net_type']
    net_name1rcr    = config_net1rcr['net_name']
    data_shape1rcr  = config_net1rcr['data_shape']
    label_shape1rcr = config_net1rcr['label_shape']
    class_num1rcr   = config_net1rcr['class_num']

    full_data_shape1rcr = [batch_size] + data_shape1rcr
    x1rcr = tf.placeholder(tf.float32, shape = full_data_shape1rcr)
    net_class1rcr = NetFactory.create(net_type1rcr)
    net1rcr = net_class1rcr(num_classes = class_num1rcr,w_regularizer = None,
                b_regularizer = None, name = net_name1rcr)
    net1rcr.set_params(config_net1rcr)
    predicty1rcr = net1rcr(x1rcr, is_training = True)
    proby1rcr = tf.nn.softmax(predicty1rcr)
    
    if(config_test.get('whole_tumor_only', False) is False):
        # 2.2, networks for tumor core
        config_net2ax = config['network2ax']
        config_net2sg = config['network2sg']
        config_net2cr = config['network2cr']
        # TODO: add three more configs -- done
        config_net2rax = config['network2rax']
        config_net2rsg = config['network2rsg']
        config_net2rcr = config['network2rcr']

        # construct graph for 2st network axial
        net_type2ax    = config_net2ax['net_type']
        net_name2ax    = config_net2ax['net_name']
        data_shape2ax  = config_net2ax['data_shape']
        label_shape2ax = config_net2ax['label_shape']
        class_num2ax   = config_net2ax['class_num']

        full_data_shape2ax = [batch_size] + data_shape2ax
        x2ax = tf.placeholder(tf.float32, shape = full_data_shape2ax)
        net_class2ax = NetFactory.create(net_type2ax)
        net2ax = net_class2ax(num_classes = class_num2ax,w_regularizer = None,
                    b_regularizer = None, name = net_name2ax)
        net2ax.set_params(config_net2ax)
        predicty2ax = net2ax(x2ax, is_training = True)
        proby2ax = tf.nn.softmax(predicty2ax)

        # construct graph for 2st network sagittal
        net_type2sg    = config_net2sg['net_type']
        net_name2sg    = config_net2sg['net_name']
        data_shape2sg  = config_net2sg['data_shape']
        label_shape2sg = config_net2sg['label_shape']
        class_num2sg   = config_net2sg['class_num']

        full_data_shape2sg = [batch_size] + data_shape2sg
        x2sg = tf.placeholder(tf.float32, shape = full_data_shape2sg)
        net_class2sg = NetFactory.create(net_type2sg)
        net2sg = net_class2sg(num_classes = class_num2sg,w_regularizer = None,
                    b_regularizer = None, name = net_name2sg)
        net2sg.set_params(config_net2sg)
        predicty2sg = net2sg(x2sg, is_training = True)
        proby2sg = tf.nn.softmax(predicty2sg)

        # construct graph for 2st network corogal
        net_type2cr    = config_net2cr['net_type']
        net_name2cr    = config_net2cr['net_name']
        data_shape2cr  = config_net2cr['data_shape']
        label_shape2cr = config_net2cr['label_shape']
        class_num2cr   = config_net2cr['class_num']

        full_data_shape2cr = [batch_size] + data_shape2cr
        x2cr = tf.placeholder(tf.float32, shape = full_data_shape2cr)
        net_class2cr = NetFactory.create(net_type2cr)
        net2cr = net_class2cr(num_classes = class_num2cr,w_regularizer = None,
                    b_regularizer = None, name = net_name2cr)
        net2cr.set_params(config_net2cr)
        predicty2cr = net2cr(x2cr, is_training = True)
        proby2cr = tf.nn.softmax(predicty2cr)

        # TODO: construct graph for 2st network rotated axial --done
        net_type2rax    = config_net2rax['net_type']
        net_name2rax    = config_net2rax['net_name']
        data_shape2rax  = config_net2rax['data_shape']
        label_shape2rax = config_net2rax['label_shape']
        class_num2rax   = config_net2rax['class_num']

        full_data_shape2rax = [batch_size] + data_shape2rax
        x2rax = tf.placeholder(tf.float32, shape = full_data_shape2rax)
        net_class2rax = NetFactory.create(net_type2rax)
        net2rax = net_class2rax(num_classes = class_num2rax,w_regularizer = None,
                    b_regularizer = None, name = net_name2rax)
        net2rax.set_params(config_net2rax)
        predicty2rax = net2rax(x2rax, is_training = True)
        proby2rax = tf.nn.softmax(predicty2rax)

        # TODO: construct graph for 2st network rotated sagittal --done
        net_type2rsg    = config_net2rsg['net_type']
        net_name2rsg    = config_net2rsg['net_name']
        data_shape2rsg  = config_net2rsg['data_shape']
        label_shape2rsg = config_net2rsg['label_shape']
        class_num2rsg   = config_net2rsg['class_num']

        full_data_shape2rsg = [batch_size] + data_shape2rsg
        x2rsg = tf.placeholder(tf.float32, shape = full_data_shape2rsg)
        net_class2rsg = NetFactory.create(net_type2rsg)
        net2rsg = net_class2rsg(num_classes = class_num2rsg,w_regularizer = None,
                    b_regularizer = None, name = net_name2rsg)
        net2rsg.set_params(config_net2rsg)
        predicty2rsg = net2rsg(x2rsg, is_training = True)
        proby2rsg = tf.nn.softmax(predicty2rsg)

        # TODO: construct graph for 2st network rotated corogal --done
        net_type2rcr    = config_net2rcr['net_type']
        net_name2rcr    = config_net2rcr['net_name']
        data_shape2rcr  = config_net2rcr['data_shape']
        label_shape2rcr = config_net2rcr['label_shape']
        class_num2rcr   = config_net2rcr['class_num']

        full_data_shape2rcr = [batch_size] + data_shape2rcr
        x2rcr = tf.placeholder(tf.float32, shape = full_data_shape2rcr)
        net_class2rcr = NetFactory.create(net_type2rcr)
        net2rcr = net_class2rcr(num_classes = class_num2rcr,w_regularizer = None,
                    b_regularizer = None, name = net_name2rcr)
        net2rcr.set_params(config_net2rcr)
        predicty2rcr = net2rcr(x2rcr, is_training = True)
        proby2rcr = tf.nn.softmax(predicty2rcr)

        # 2.3, networks for enhanced tumor
        config_net3ax = config['network3ax']
        config_net3sg = config['network3sg']
        config_net3cr = config['network3cr']
        # TODO: add three more configs --done
        config_net3rax = config['network3rax']
        config_net3rsg = config['network3rsg']
        config_net3rcr = config['network3rcr']

        # construct graph for 3rd network axial
        net_type3ax    = config_net3ax['net_type']
        net_name3ax    = config_net3ax['net_name']
        data_shape3ax  = config_net3ax['data_shape']
        label_shape3ax = config_net3ax['label_shape']
        class_num3ax   = config_net3ax['class_num']

        full_data_shape3ax = [batch_size] + data_shape3ax
        x3ax = tf.placeholder(tf.float32, shape = full_data_shape3ax)
        net_class3ax = NetFactory.create(net_type3ax)
        net3ax = net_class3ax(num_classes = class_num3ax,w_regularizer = None,
                    b_regularizer = None, name = net_name3ax)
        net3ax.set_params(config_net3ax)
        predicty3ax = net3ax(x3ax, is_training = True)
        proby3ax = tf.nn.softmax(predicty3ax)

        # construct graph for 3rd network sagittal
        net_type3sg    = config_net3sg['net_type']
        net_name3sg    = config_net3sg['net_name']
        data_shape3sg  = config_net3sg['data_shape']
        label_shape3sg = config_net3sg['label_shape']
        class_num3sg   = config_net3sg['class_num']
        # construct graph for 3rd network
        full_data_shape3sg = [batch_size] + data_shape3sg
        x3sg = tf.placeholder(tf.float32, shape = full_data_shape3sg)
        net_class3sg = NetFactory.create(net_type3sg)
        net3sg = net_class3sg(num_classes = class_num3sg,w_regularizer = None,
                    b_regularizer = None, name = net_name3sg)
        net3sg.set_params(config_net3sg)
        predicty3sg = net3sg(x3sg, is_training = True)
        proby3sg = tf.nn.softmax(predicty3sg)

        # construct graph for 3rd network corogal
        net_type3cr    = config_net3cr['net_type']
        net_name3cr    = config_net3cr['net_name']
        data_shape3cr  = config_net3cr['data_shape']
        label_shape3cr = config_net3cr['label_shape']
        class_num3cr   = config_net3cr['class_num']
        # construct graph for 3rd network
        full_data_shape3cr = [batch_size] + data_shape3cr
        x3cr = tf.placeholder(tf.float32, shape = full_data_shape3cr)
        net_class3cr = NetFactory.create(net_type3cr)
        net3cr = net_class3cr(num_classes = class_num3cr,w_regularizer = None,
                    b_regularizer = None, name = net_name3cr)
        net3cr.set_params(config_net3cr)
        predicty3cr = net3cr(x3cr, is_training = True)
        proby3cr = tf.nn.softmax(predicty3cr)

        # TODO: construct graph for 3rd network rotated axial --done
        net_type3rax    = config_net3rax['net_type']
        net_name3rax    = config_net3rax['net_name']
        data_shape3rax  = config_net3rax['data_shape']
        label_shape3rax = config_net3rax['label_shape']
        class_num3rax   = config_net3rax['class_num']

        full_data_shape3rax = [batch_size] + data_shape3rax
        x3rax = tf.placeholder(tf.float32, shape = full_data_shape3rax)
        net_class3rax = NetFactory.create(net_type3rax)
        net3rax = net_class3rax(num_classes = class_num3rax,w_regularizer = None,
                    b_regularizer = None, name = net_name3rax)
        net3rax.set_params(config_net3rax)
        predicty3rax = net3rax(x3rax, is_training = True)
        proby3rax = tf.nn.softmax(predicty3rax)

        # TODO: construct graph for 3rd network rotated sagittal --done
        net_type3rsg    = config_net3rsg['net_type']
        net_name3rsg    = config_net3rsg['net_name']
        data_shape3rsg  = config_net3rsg['data_shape']
        label_shape3rsg = config_net3rsg['label_shape']
        class_num3rsg   = config_net3rsg['class_num']

        full_data_shape3rsg = [batch_size] + data_shape3rsg
        x3rsg = tf.placeholder(tf.float32, shape = full_data_shape3rsg)
        net_class3rsg = NetFactory.create(net_type3rsg)
        net3rsg = net_class3rsg(num_classes = class_num3rsg,w_regularizer = None,
                    b_regularizer = None, name = net_name3rsg)
        net3rsg.set_params(config_net3rsg)
        predicty3rsg = net3rsg(x3rsg, is_training = True)
        proby3rsg = tf.nn.softmax(predicty3rsg)

        # TODO: construct graph for 3rd network rotated corogal --done
        net_type3rcr    = config_net3rcr['net_type']
        net_name3rcr    = config_net3rcr['net_name']
        data_shape3rcr  = config_net3rcr['data_shape']
        label_shape3rcr = config_net3rcr['label_shape']
        class_num3rcr   = config_net3rcr['class_num']

        full_data_shape3rcr = [batch_size] + data_shape3rcr
        x3rcr = tf.placeholder(tf.float32, shape = full_data_shape3rcr)
        net_class3rcr = NetFactory.create(net_type3rcr)
        net3rcr = net_class3rcr(num_classes = class_num3rcr,w_regularizer = None,
                    b_regularizer = None, name = net_name3rcr)
        net3rcr.set_params(config_net3rcr)
        predicty3rcr = net3rcr(x3rcr, is_training = True)
        proby3rcr = tf.nn.softmax(predicty3rcr)

    # 3, create session and load trained models
    all_vars = tf.global_variables()
    sess = tf.InteractiveSession()   
    sess.run(tf.global_variables_initializer())  

    net1ax_vars = [x for x in all_vars if x.name[0:len(net_name1ax) + 1]==net_name1ax + '/']
    saver1ax = tf.train.Saver(net1ax_vars)
    saver1ax.restore(sess, config_net1ax['model_file'])
    net1sg_vars = [x for x in all_vars if x.name[0:len(net_name1sg) + 1]==net_name1sg + '/']
    saver1sg = tf.train.Saver(net1sg_vars)
    saver1sg.restore(sess, config_net1sg['model_file'])
    net1cr_vars = [x for x in all_vars if x.name[0:len(net_name1cr) + 1]==net_name1cr + '/']
    saver1cr = tf.train.Saver(net1cr_vars)
    saver1cr.restore(sess, config_net1cr['model_file'])
    # TODO: load three more 1st stage models --done
    net1rax_vars = [x for x in all_vars if x.name[0:len(net_name1rax) + 1]==net_name1rax + '/']
    saver1rax = tf.train.Saver(net1rax_vars)
    saver1rax.restore(sess, config_net1rax['model_file'])
    net1rsg_vars = [x for x in all_vars if x.name[0:len(net_name1rsg) + 1]==net_name1rsg + '/']
    saver1rsg = tf.train.Saver(net1rsg_vars)
    saver1rsg.restore(sess, config_net1rsg['model_file'])
    net1rcr_vars = [x for x in all_vars if x.name[0:len(net_name1rcr) + 1]==net_name1rcr + '/']
    saver1rcr = tf.train.Saver(net1rcr_vars)
    saver1rcr.restore(sess, config_net1rcr['model_file'])

    if(config_test.get('whole_tumor_only', False) is False):

        net2ax_vars = [x for x in all_vars if x.name[0:len(net_name2ax)+1]==net_name2ax + '/']
        saver2ax = tf.train.Saver(net2ax_vars)
        saver2ax.restore(sess, config_net2ax['model_file'])
        net2sg_vars = [x for x in all_vars if x.name[0:len(net_name2sg)+1]==net_name2sg + '/']
        saver2sg = tf.train.Saver(net2sg_vars)
        saver2sg.restore(sess, config_net2sg['model_file'])
        net2cr_vars = [x for x in all_vars if x.name[0:len(net_name2cr)+1]==net_name2cr + '/']
        saver2cr = tf.train.Saver(net2cr_vars)
        saver2cr.restore(sess, config_net2cr['model_file'])
        # TODO: load three more 2nd stage models --done
        net2rax_vars = [x for x in all_vars if x.name[0:len(net_name2rax)+1]==net_name2rax + '/']
        saver2rax = tf.train.Saver(net2rax_vars)
        saver2rax.restore(sess, config_net2rax['model_file'])
        net2rsg_vars = [x for x in all_vars if x.name[0:len(net_name2rsg)+1]==net_name2rsg + '/']
        saver2rsg = tf.train.Saver(net2rsg_vars)
        saver2rsg.restore(sess, config_net2rsg['model_file'])
        net2rcr_vars = [x for x in all_vars if x.name[0:len(net_name2rcr)+1]==net_name2rcr + '/']
        saver2rcr = tf.train.Saver(net2rcr_vars)
        saver2rcr.restore(sess, config_net2rcr['model_file'])

        net3ax_vars = [x for x in all_vars if x.name[0:len(net_name3ax) + 1]==net_name3ax+ '/']
        saver3ax = tf.train.Saver(net3ax_vars)
        saver3ax.restore(sess, config_net3ax['model_file'])
        net3sg_vars = [x for x in all_vars if x.name[0:len(net_name3sg) + 1]==net_name3sg+ '/']
        saver3sg = tf.train.Saver(net3sg_vars)
        saver3sg.restore(sess, config_net3sg['model_file'])
        net3cr_vars = [x for x in all_vars if x.name[0:len(net_name3cr) + 1]==net_name3cr+ '/']
        saver3cr = tf.train.Saver(net3cr_vars)
        saver3cr.restore(sess, config_net3cr['model_file'])
        # TODO: load three more 3rd stage models
        net3rax_vars = [x for x in all_vars if x.name[0:len(net_name3rax) + 1]==net_name3rax+ '/']
        saver3rax = tf.train.Saver(net3rax_vars)
        saver3rax.restore(sess, config_net3rax['model_file'])
        net3rsg_vars = [x for x in all_vars if x.name[0:len(net_name3rsg) + 1]==net_name3rsg+ '/']
        saver3rsg = tf.train.Saver(net3rsg_vars)
        saver3rsg.restore(sess, config_net3rsg['model_file'])
        net3rcr_vars = [x for x in all_vars if x.name[0:len(net_name3rcr) + 1]==net_name3rcr+ '/']
        saver3rcr = tf.train.Saver(net3rcr_vars)
        saver3rcr.restore(sess, config_net3rcr['model_file'])

    # 4, load test images
    # TODO: [VERY IMPORTANT]
    #       We should re-implement it,
    #       but the workflow should be the same.
    #       The Inf_Dataloader is the file we should create,
    #       refering to the original Dataloader instead of the modified one
    dataloader = Inf_DataLoader(config_data)
    # TODO: dataloader would load two sets of data: original data and rotated data
    dataloader.load_data()
    image_num = dataloader.get_total_image_number()
    
    # 5, start to test
    test_slice_direction = config_test.get('test_slice_direction', 'all')
    save_folder = config_data['save_folder']
    test_time = []
    struct = ndimage.generate_binary_structure(3, 2)
    margin = config_test.get('roi_patch_margin', 5)
    
    for i in range(image_num):
        # TODO: get the original data, get the rotated data -- done
        (data_ori, data_r) = dataloader.get_image_data_with_name(i)
        [temp_imgs, temp_weight, temp_name, img_names, temp_bbox, temp_size] = data_ori
        [temp_imgs_r, temp_weight_r, temp_name_r, img_names_r, temp_bbox_r, temp_size_r] = data_r
        t0 = time.time()
        # 5.1, test of 1st network
        data_shapes  = [ data_shape1ax[:-1],  data_shape1sg[:-1],  data_shape1cr[:-1]]
        label_shapes = [label_shape1ax[:-1], label_shape1sg[:-1], label_shape1cr[:-1]]
        nets = [net1ax, net1sg, net1cr]
        outputs = [proby1ax, proby1sg, proby1cr]
        inputs =  [x1ax, x1sg, x1cr]

        class_num = class_num1ax
        prob1 = test_one_image_three_nets_adaptive_shape(temp_imgs, data_shapes, label_shapes, data_shape1ax[-1], class_num,
                   batch_size, sess, nets, outputs, inputs, shape_mode = 2)
        pred1 =  np.asarray(np.argmax(prob1, axis = 3), np.uint16)
        pred1 = pred1 * temp_weight

        # TODO: test of rotated 1st network, using rotated data
        #       return pred1_r, same shape as that of pred1_r -- done

        data_shapes_r  = [ data_shape1rax[:-1],  data_shape1rsg[:-1],  data_shape1rcr[:-1]]
        label_shapes_r = [label_shape1rax[:-1], label_shape1rsg[:-1], label_shape1rcr[:-1]]
        nets_r = [net1rax, net1rsg, net1rcr]
        outputs_r = [proby1rax, proby1rsg, proby1rcr]
        inputs_r =  [x1rax, x1rsg, x1rcr]

        class_num_r = class_num1rax
        prob1_r = test_one_image_three_nets_adaptive_shape(temp_imgs_r, data_shapes_r, label_shapes_r, data_shape1rax[-1], class_num_r,
                   batch_size, sess, nets_r, outputs_r, inputs_r, shape_mode = 2)
        pred1_r =  np.asarray(np.argmax(prob1_r, axis = 3), np.uint16)
        pred1_r = pred1_r * temp_weight_r

        wt_threshold = 2000
        if(config_test.get('whole_tumor_only', False) is True):
            pred1_lc = ndimage.morphology.binary_closing(pred1, structure = struct)
            pred1_lc = get_largest_two_component(pred1_lc, False, wt_threshold)
            out_label = pred1_lc
            # TODO: do morphology trnasform on rotated pred1_r
            #       return out_label_r --done

            pred1_lc_r = ndimage.morphology.binary_closing(pred1_r, structure = struct)
            pred1_lc_r = get_largest_two_component(pred1_lc_r, False, wt_threshold)
            out_label_r = pred1_lc_r
        else:
            # 5.2, test of 2nd network
            if(pred1.sum() == 0):
                print('net1 output is null', temp_name)
                bbox1 = get_ND_bounding_box(temp_imgs[0] > 0, margin)
                # TODO: do the similar method on rotated temp imgs, boring...
                #       return bbox1_r -- done
                # Aradhya's comment: i have implemented it after the else loop in the if statement
            else:
                pred1_lc = ndimage.morphology.binary_closing(pred1, structure = struct)
                pred1_lc = get_largest_two_component(pred1_lc, False, wt_threshold)
                bbox1 = get_ND_bounding_box(pred1_lc, margin)
                # TODO: get the bbox1_r -- done
                # Aradhya's comment: i have implemented it in the if after this else
            if(pred1_r.sum() == 0):
                print('net1 output is null', temp_name_r)
                bbox1_r = get_ND_bounding_box(temp_imgs_r[0] > 0, margin)
            else:
                pred1_lc_r = ndimage.morphology.binary_closing(pred1_r, structure = struct)
                pred1_lc_r = get_largest_two_component(pred1_lc_r, False, wt_threshold)
                bbox1_r = get_ND_bounding_box(pred1_lc_r, margin)
            
            sub_imgs = [crop_ND_volume_with_bounding_box(one_img, bbox1[0], bbox1[1]) for one_img in temp_imgs]
            sub_weight = crop_ND_volume_with_bounding_box(temp_weight, bbox1[0], bbox1[1])

            data_shapes  = [ data_shape2ax[:-1],  data_shape2sg[:-1],  data_shape2cr[:-1]]
            label_shapes = [label_shape2ax[:-1], label_shape2sg[:-1], label_shape2cr[:-1]]
            nets = [net2ax, net2sg, net2cr]
            outputs = [proby2ax, proby2sg, proby2cr]
            inputs =  [x2ax, x2sg, x2cr]
            class_num = class_num2ax

            prob2 = test_one_image_three_nets_adaptive_shape(sub_imgs, data_shapes, label_shapes, data_shape2ax[-1],
                class_num,  batch_size, sess, nets, outputs, inputs, shape_mode = 1)
            pred2 = np.asarray(np.argmax(prob2, axis = 3), np.uint16)
            pred2 = pred2 * sub_weight

            # TODO: get the rotated sub meta-data like above -- done

            sub_imgs_r = [crop_ND_volume_with_bounding_box(one_img_r, bbox1_r[0], bbox1_r[1]) for one_img_r in temp_imgs_r]
            sub_weight_r = crop_ND_volume_with_bounding_box(temp_weight_r, bbox1_r[0], bbox1_r[1])

            data_shapes_r  = [ data_shape2rax[:-1], data_shape2sg[:-1],  data_shape2rcr[:-1]]
            label_shapes_r = [label_shape2rax[:-1], label_shape2rsg[:-1], label_shape2rcr[:-1]]
            nets_r = [net2rax, net2rsg, net2rcr]
            outputs_r = [proby2rax, proby2rsg, proby2rcr]
            inputs_r =  [x2rax, x2rsg, x2rcr]
            class_num_r = class_num2rax

            # TODO: test of rotated 2nd network, using rotated sub data
            #       return pred2_r, same shape as that of pred2 -- done
            prob2_r = test_one_image_three_nets_adaptive_shape(sub_imgs_r, data_shapes_r, label_shapes_r, data_shape2rax[-1],
                class_num_r,  batch_size, sess, nets_r, outputs_r, inputs_r, shape_mode = 1)
            pred2_r = np.asarray(np.argmax(prob2_r, axis = 3), np.uint16)
            pred2_r = pred2_r * sub_weight_r
             
            # 5.3, test of 3rd network
            if(pred2.sum() == 0):
                [roid, roih, roiw] = sub_imgs[0].shape
                bbox2 = [[0,0,0], [roid-1, roih-1, roiw-1]]
                subsub_imgs = sub_imgs
                subsub_weight = sub_weight
                # TODO: do the similar method on rotated temp sub imgs, boring...
                #       return bbox2_r, subsub data -- done
                # Aradhya's comment : implemented it in the next if after else
            else:
                pred2_lc = ndimage.morphology.binary_closing(pred2, structure = struct)
                pred2_lc = get_largest_two_component(pred2_lc)
                bbox2 = get_ND_bounding_box(pred2_lc, margin)
                subsub_imgs = [crop_ND_volume_with_bounding_box(one_img, bbox2[0], bbox2[1]) for one_img in sub_imgs]
                subsub_weight = crop_ND_volume_with_bounding_box(sub_weight, bbox2[0], bbox2[1])
                # TODO: get the bbox2_r and subsub data -- done
                # Aradhya's comment : implemented it in the next if this else

            if(pred2_r.sum() == 0):
                [roid_r, roih_r, roiw_r] = sub_imgs_r[0].shape
                bbox2_r = [[0,0,0], [roid_r-1, roih_r-1, roiw_r-1]]
                subsub_imgs_r = sub_imgs_r
                subsub_weight_r = sub_weight_r
            else:
                pred2_lc_r = ndimage.morphology.binary_closing(pred2_r, structure = struct)
                pred2_lc_r = get_largest_two_component(pred2_lc_r)
                bbox2_r = get_ND_bounding_box(pred2_lc_r, margin)
                subsub_imgs_r = [crop_ND_volume_with_bounding_box(one_img_r, bbox2_r[0], bbox2_r[1]) for one_img_r in sub_imgs_r]
                subsub_weight_r = crop_ND_volume_with_bounding_box(sub_weight_r, bbox2_r[0], bbox2_r[1])

            data_shapes  = [ data_shape3ax[:-1],  data_shape3sg[:-1],  data_shape3cr[:-1]]
            label_shapes = [label_shape3ax[:-1], label_shape3sg[:-1], label_shape3cr[:-1]]
            nets = [net3ax, net3sg, net3cr]
            outputs = [proby3ax, proby3sg, proby3cr]
            inputs =  [x3ax, x3sg, x3cr]
            class_num = class_num3ax

            prob3 = test_one_image_three_nets_adaptive_shape(subsub_imgs, data_shapes, label_shapes, data_shape3ax[-1],
                class_num, batch_size, sess, nets, outputs, inputs, shape_mode = 1)

            pred3 = np.asarray(np.argmax(prob3, axis = 3), np.uint16)
            pred3 = pred3 * subsub_weight

            # TODO: get the rotated subsub meta-data like above -- done
            data_shapes_r  = [ data_shape3rax[:-1],  data_shape3rsg[:-1],  data_shape3rcr[:-1]]
            label_shapes_r = [label_shape3rax[:-1], label_shape3rsg[:-1], label_shape3rcr[:-1]]
            nets_r = [net3rax, net3rsg, net3rcr]
            outputs_r = [proby3rax, proby3rsg, proby3rcr]
            inputs_r =  [x3rax, x3rsg, x3rcr]
            class_num_r = class_num3rax

            # TODO: test of rotated 3rd network, using rotated subsub data
            #       return pred_3r, same shape as that of pred_3 -- done
            prob3_r = test_one_image_three_nets_adaptive_shape(subsub_imgs_r, data_shapes_r, label_shapes_r, data_shape3rax[-1],
                class_num_r, batch_size, sess, nets_r, outputs_r, inputs_r, shape_mode = 1)

            pred3_r = np.asarray(np.argmax(prob3_r, axis = 3), np.uint16)
            pred3_r = pred3_r * subsub_weight_r
             
            # 5.4, fuse results at 3 levels
            # convert subsub_label to full size (non-enhanced)
            label3_roi = np.zeros_like(pred2)
            label3_roi = set_ND_volume_roi_with_bounding_box_range(label3_roi, bbox2[0], bbox2[1], pred3)
            label3 = np.zeros_like(pred1)
            label3 = set_ND_volume_roi_with_bounding_box_range(label3, bbox1[0], bbox1[1], label3_roi)
            # TODO: get label3_r -- done
            label3_roi_r = np.zeros_like(pred2_r)
            label3_roi_r = set_ND_volume_roi_with_bounding_box_range(label3_roi_r, bbox2_r[0], bbox2_r[1], pred3_r)
            label3_r = np.zeros_like(pred1_r)
            label3_r = set_ND_volume_roi_with_bounding_box_range(label3_r, bbox1_r[0], bbox1_r[1], label3_roi_r)

            label2 = np.zeros_like(pred1)
            label2 = set_ND_volume_roi_with_bounding_box_range(label2, bbox1[0], bbox1[1], pred2)
            # TODO: get label2_r -- done
            label2_r = np.zeros_like(pred1_r)
            label2_r = set_ND_volume_roi_with_bounding_box_range(label2_r, bbox1_r[0], bbox1_r[1], pred2_r)

            label1_mask = (pred1 + label2 + label3) > 0
            label1_mask = ndimage.morphology.binary_closing(label1_mask, structure = struct)
            label1_mask = get_largest_two_component(label1_mask, False, wt_threshold)
            label1 = pred1 * label1_mask
            # TODO: get label1_r --done
            label1_mask_r = (pred1_r + label2_r + label3_r) > 0
            label1_mask_r = ndimage.morphology.binary_closing(label1_mask_r, structure = struct)
            label1_mask_r = get_largest_two_component(label1_mask_r, False, wt_threshold)
            label1_r = pred1_r * label1_mask_r
            
            label2_3_mask = (label2 + label3) > 0
            label2_3_mask = label2_3_mask * label1_mask
            label2_3_mask = ndimage.morphology.binary_closing(label2_3_mask, structure = struct)
            label2_3_mask = remove_external_core(label1, label2_3_mask)
            if(label2_3_mask.sum() > 0):
                label2_3_mask = get_largest_two_component(label2_3_mask)
            # TODO: get the label2_3_mask_r -- done
            label2_3_mask_r = (label2_r + label3_r) > 0
            label2_3_mask_r = label2_3_mask_r * label1_mask_r
            label2_3_mask_r = ndimage.morphology.binary_closing(label2_3_mask_r, structure = struct)
            label2_3_mask_r = remove_external_core(label1_r, label2_3_mask_r)
            if(label2_3_mask_r.sum() > 0):
                label2_3_mask_r = get_largest_two_component(label2_3_mask_r)

            label1 = (label1 + label2_3_mask) > 0
            label2 = label2_3_mask
            label3 = label2 * label3
            vox_3  = np.asarray(label3 > 0, np.float32).sum()
            if(0 < vox_3 and vox_3 < 30):
                label3 = np.zeros_like(label2)
            # TODO: get the label3_r -- done
            label1_r = (label1_r + label2_3_mask_r) > 0
            label2_r = label2_3_mask_r
            label3_r = label2_r * label3_r
            vox_3_r  = np.asarray(label3_r > 0, np.float32).sum()
            if(0 < vox_3_r and vox_3_r < 30):
                label3_r = np.zeros_like(label2_r)

            # TODO: fuse the rotated pred result and original pred result
            #       let's figure out how to do it, return label1, label2, label3

            # 5.5, convert label and save output
            out_label = label1 * 2
            if('Flair' in config_data['modality_postfix'] and 'mha' in config_data['file_postfix']):
                out_label[label2>0] = 3
                out_label[label3==1] = 1
                out_label[label3==2] = 4
            elif('flair' in config_data['modality_postfix'] and 'nii' in config_data['file_postfix']):
                out_label[label2>0] = 1
                out_label[label3>0] = 4
            out_label = np.asarray(out_label, np.int16)


        test_time.append(time.time() - t0)
        final_label = np.zeros(temp_size, np.int16)
        final_label = set_ND_volume_roi_with_bounding_box_range(final_label, temp_bbox[0], temp_bbox[1], out_label)
        save_array_as_nifty_volume(final_label, save_folder+"/{0:}.nii.gz".format(temp_name), img_names[0])
        print(temp_name)
    test_time = np.asarray(test_time)
    print('test time', test_time.mean())
    np.savetxt(save_folder + '/test_time.txt', test_time)
    sess.close()
      
if __name__ == '__main__':
    if(len(sys.argv) != 2):
        print('Number of arguments should be 2. e.g.')
        print('    python test.py config17/test_all_class.txt')
        exit()
    config_file = str(sys.argv[1])
    assert(os.path.isfile(config_file))
    test(config_file)
    
    
